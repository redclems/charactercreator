class CharacterFetch {
    /**
     * Fetch the list of characters
     * @param {int} page the page
     * @param {AppCharacter} listener
     * @returns {Array<Character>} the list of characters
     */
    static async getCharacterList(page, listener) {
        const nbItemByPage = 5;
        const res = await fetch("http://localhost:3000/characters", {
            method: "GET",
        }).then(response => response.json());

        let characterList = [];
        let nb = 0;
        for (const c of res) {
          if(nb >= nbItemByPage*page && nb < nbItemByPage + (nbItemByPage*page)){
            characterList.push(new Character(c.id, c.name, c.surname, c.description, c.game, c.items, c.level, c.uri));
          }
          nb++;
        }

        let plus = 0;
        if(nb%nbItemByPage != 0){
          plus++;
        }

        listener.pageBp((nb/nbItemByPage) + plus);


        return characterList;
    }

    /**
     * Fetch one character depending on its ID
     *
     * @param {int} id the id of the character
     * @returns {Character} the corresponding character
     */
    static async getCharacterById(id) {
        const c = await fetch("http://localhost:3000/characters/"+id, {
            method: "GET",
        }).then(response => response.json());
        return new Character(c.id, c.name, c.surname, c.description, c.game, c.items, c.level, c.uri);
    }    /**
     * Fetch all character depending on its research
     * @param {int} page the page
     * @param {AppCharacter} listener
     * @param {string} research the research of the character
     * @returns {Character} the corresponding character
     */
    static async getCharacterResearch(research, page, listener) {
      const nbItemByPage = 5;
      const res = await fetch("http://localhost:3000/characters", {
          method: "GET",
      }).then(response => response.json());

      let characterList = [];
      let nb = 0;
      for (const c of res){
        if(nb >= nbItemByPage*page && nb < nbItemByPage + (nbItemByPage*page)){
          if(c.id === research || c.name === research || c.surname === research  || c.game === research || research == ""){
            characterList.push(new Character(c.id, c.name, c.surname, c.description, c.game, c.items, c.level, c.uri));
          }
        }
        nb++;
      }

      let plus = 0;
      if(nb%nbItemByPage != 0){
        plus++;
      }

      listener.pageBp((nb/nbItemByPage) + plus);

      return characterList;

    }

    /**
     * Inserts a new character
     *
     * @param {Character} character
     */
    static async insertCharacter(character) {
        let newId = await this.getMaxId();
        if (character.id < 0) character.setId(newId+1);
        character.newUri(newId);
        const c = await fetch("http://localhost:3000/characters", {
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json"
            },
            method: "POST",
            body: JSON.stringify(character)
        }).then(response => {
            if (response.ok) {
                console.log("Save Success");
            }
            else throw new Error("Ajax error: "+response.status);
        })
        .catch(response => {console.log(response)});
    }

    /**
     * Deletes one character depending on its ID
     *
     * @param {int} id the id of the character
     * @returns {Promise<Character>} the character that has been deleted
     */
    static async deleteCharacter(id) {
        return fetch("http://localhost:3000/characters/"+id, {
            method: "DELETE",
        }).then(response => response.json());
    }

    /**
     * Fetch the characters and returns the highest ID
     *
     * @returns {int} the highest ID
    */
    static async getMaxId() {
        const res = await fetch("http://localhost:3000/characters", {
            method: "GET",
        }).then(response => response.json());

        let characterList = [];
        for (const c of res) characterList.push(c.id);

        console.log(Math.max(characterList));
        return Math.max.apply(null,characterList)
    }

    /**
     * Replaces the character that has the same ID as the parameter
     *
     * @param {Character} character
     */
    static async modifyCharacter(character) {
        console.log(JSON.stringify(character));
        const c = await fetch("http://localhost:3000/characters/"+character.id, {
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json"
            },
            method: "PUT",
            body: JSON.stringify(character)
        }).then(response => {
            if (response.ok) {
                console.log("Save Success");
            }
            else throw new Error("Ajax error: "+response.status);
        })
        .catch(response => {console.log(response)});
    }
}

class Character {
  constructor(newId, newName, newSurname, newDescription, newGame, newItems, newLevel, newUri) {
        this.id = newId;

        this.name = newName;
        this.surname = newSurname;
        this.description = newDescription;
        this.game = newGame;
        this.items = newItems;
        this.level = newLevel;
        this.uri = newUri;
    }

    get newId() {
        return this.id;

    }

    get newName() {
        return this.name;
    }

    get newSurname() {
        return this.surname;
    }

    get newDescription() {
        return this.description;
    }

    get newGame() {
        return this.game;
    }

    get newItems() {
        return this.items;
    }

    get newLevel() {
        return this.level;
    }

    get newUri() {
        return this.uri;
    }

    async newUri(id) {
        this.uri = "http://localhost:3000/characters/"+(id+1);
    }

    setId(newId) {
        this.id = newId;
    }
    setUri(newUri) {
        this.uri = newUri;
    }
}
