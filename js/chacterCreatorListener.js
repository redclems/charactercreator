class AppCharacter {

  constructor() {
      this._title = document.getElementById('title');
      this._changeList = document.getElementById('changeList');
      this._addItems = document.getElementById('addItems');
      this._addCharacter = document.getElementById('addCharacter');

      this._containerListLeft = document.getElementById('containerListLeft');
      this._titleList = document.getElementById('titleList');
      this._buttonRefresh = document.getElementById("buttonRefresh");
      this._listLeft = document.getElementById("listLeft");
      this._paginationLeft = document.getElementById("paginationLeft");


      this._containerView = document.getElementById('containerView');
      this._titleView = document.getElementById('titleView');
      this._tools = document.getElementById("tools");
      this._add = document.getElementById("add");
      this._del = document.getElementById("del");
      this._currenttask = document.getElementById("currenttask");

      this._research = document.getElementById("research");

      this._editColor = document.getElementById("editColor");

      this._FormItem = new FormItem(this);
      this._FormCharacter = new FormCharacter(this);

      this._fetchChar = new CharacterFetch();

      const self = this;

      this._changeList.onclick = (e) => self.switchList(e);
      this._addItems.onclick = (e) => self.addItem(e);
      this._addCharacter.onclick = (e) => self.addCharacter(e);
      this._title.onclick = (e) => self.bindAll();
      this._add.onclick = (e) => self.bpAdd();
      this._del.onclick  = (e) => self.del();
      this._buttonRefresh.onclick = (e) => self.viewList();
      this._research.onchange = (e) => self.researchList();

      this._editColor.onclick = (e) => self.changeStyle();
  }


  changeStyle(){
      var stylesheet = document.getElementById("colorCSS");
      if(stylesheet.getAttribute('href') == 'css/color.css')
        stylesheet.setAttribute('href','css/color2.css');
      else if(stylesheet.getAttribute('href') == 'css/color2.css')
        stylesheet.setAttribute('href','css/color3.css');
      else stylesheet.setAttribute('href','css/color.css');

  }
  /*
  *switch the list
  *
  */

  switchList(element){
      this._FormCharacter.viewClear();
      this._FormItem.viewClear();
      if(this._titleList.innerText === "Liste des personnages"){
          this._titleList.innerText = "Liste des objets";
          this._changeList.value = "Liste des personnages";
      }else{
          this._titleList.innerText = "Liste des personnages";
          this._changeList.value = "Liste des objets";
      }
      this.viewList()
  }

  async viewList(){
    let listChar = [];
    let page = 0;
    if(this._titleList.innerText === "Liste des personnages"){
        listChar = await CharacterFetch.getCharacterList(page, this);
    }else{
        listChar = await ItemFetch.getItemList(page, this);
    }
    //console.log(listChar);
    this.createListLeft(listChar);
  }

  async researchList(){
    let listChar = [];
    let page = 0;
    if(this._titleList.innerText === "Liste des personnages"){
        listChar = await CharacterFetch.getCharacterResearch(this._research.value, page, this);
    }else{
        listChar = await ItemFetch.getItemResearch(this._research.value, page, this);
    }
    this.createListLeft(listChar)
  }

  pageBp(nbPage){
    if(nbPage > 0){
      let ul = document.createElement("ul");
      ul.id = "ulPagination";
      ul.className =  "flex";
      for (let i = 1; i <= nbPage; i++) {
        //console.log(element);
        let li = document.createElement("li");

        li.id = i-1;
        li.innerText = i;
        li.className = "pageNume";
        li.onclick  = (e) => this.pageUpdate(e);

        ul.appendChild(li);
      }
      this._paginationLeft.appendChild(ul);
    }
  }

  async pageUpdate(element){
    let page = element.target.id;
    console.log("bouton click" + page);
    let listChar = [];
    if(this._titleList.innerText === "Liste des personnages"){
        listChar = await CharacterFetch.getCharacterResearch(this._research.value, page, this);
    }else{
        listChar = await ItemFetch.getItemResearch(this._research.value, page, this);
    }
    this.createListLeft(listChar)
  }

  createListLeft(listVal){
    //console.log(listChar);
    let ul = document.createElement("ul");
    ul.id = "ulStock";
    for (const element of listVal) {
      //console.log(element);
      let li;
      if(this._titleList.innerText === "Liste des personnages"){
          li = this.displayElementListCharacter(element);
      }else{
          li = this.displayElementListItem(element);
      }
      ul.appendChild(li);
    }
    if (this._listLeft.hasChildNodes()) {
      let children = this._listLeft.childNodes;

      for (let i = 0; i < children.length; i++) {
        this._listLeft.removeChild(children[i]);
      }

    }
    if (this._paginationLeft.hasChildNodes()) {
      let children = this._paginationLeft.childNodes;

      for (let i = 0; i < children.length; i++) {
        this._paginationLeft.removeChild(children[i]);
      }

    }

    this._listLeft.appendChild(ul);
  }

  displayElementListCharacter(character){
    let li = document.createElement("li");

    li.id = character.id;
    li.innerText = character.name + " (" + character.surname + ") ";
    li.onclick  = (e) => this.editCharacter(e);

    return li;
  }
  displayElementListItem(item){
    let li = document.createElement("li");

    li.id = item.id;
    li.innerText = item.name;
    li.onclick  = (e) => this.editItem(e);

    return li;
  }

  async editCharacter(element){

    let li = element.target;
    //console.log(li.id);
    this.bindAll();
    this._FormCharacter.displayForm();
    this._FormCharacter.viewAll(await CharacterFetch.getCharacterById(li.id));
  }

  async editItem(element){

    let li = element.target;
    //console.log(li.id);
    this.bindAll();
    this._FormItem.displayForm();
    this._FormItem.viewAll(await ItemFetch.getItemById(li.id));
  }



  bindAll(){
    this._FormItem.bindForm();
    this._FormCharacter.bindForm();
  }

  removeChild(){
    let listChildListLeft = this._containerListLeft.childNodes;
    for(const element of listChildListLeft){
      this._containerListLeft.removeChild(element);
    }
  }

  bpAdd(){
    this._FormCharacter.viewClear();
    this._FormItem.viewClear();
    if(this._titleList.innerText == "Liste des personnages"){
        this.addCharacter();
    }else{
        this.addItem();
    }
  }

  addCharacter(element){
    this._FormCharacter.viewClear();
    this._FormItem.viewClear();
    this._FormItem.bindForm();
    this._FormCharacter.displayForm();
  }

  addItem(element){
    this._FormCharacter.viewClear();
    this._FormItem.viewClear();
    this._FormCharacter.bindForm();
    this._FormItem.displayForm();
  }

  del(){
    if(document.getElementById("formCharacter").style.display === "block"){
      this.delCharacter();
    }else if(document.getElementById("formItem").style.display === "block"){
      this.delItem();
    }
  }

  delItem(element){
    console.log("itemmm");
    this._FormItem.bindForm();
    this._FormItem.bindForm();
    this._FormItem.del()
  }

  delCharacter(element){
    console.log("charaa");
    this._FormItem.bindForm();
    this._FormCharacter.bindForm();
    this._FormCharacter.del()
  }


  async load() {
    console.log("load");
    this._FormItem.bindForm();
    this._FormCharacter.bindForm();
  }


}


window.onload = async () => {
    console.log("start");
    const app = new AppCharacter();

    await app.load();
}
