class FormCharacter {
  constructor(listener) {
    this._formCharacter = document.getElementById("formCharacter");
    this._user_id_li = document.getElementById("user_id_li");
    this._user_id = document.getElementById("user_id");
    this._user_name = document.getElementById("user_name");
    this._user_surname = document.getElementById("user_surname");
    this._user_description = document.getElementById("user_description");
    this._user_game = document.getElementById("user_game");
    this._li_checkbox = document.getElementById("li_checkbox");
    this._user_level = document.getElementById("user_level");
    this._itemList = document.getElementById("li_checkbox");
    this._validateFormCharact = document.getElementById("validateFormCharact");

    const self = this;

    this._listener = listener;

    this._validateFormCharact.onclick = (e) => self.submit();

  }
  bindForm(){
      this._formCharacter.style.display = "none";
  }

  displayForm(){
    this._formCharacter.style.display = "block";
    this._user_id_li.style.display = "none";
  }

  async del(){
    c//onsole.log("del " + this._user_id.value);
    await CharacterFetch.deleteCharacter(this._user_id.value);
    this._listener.viewList();
  }

  async viewAll(character){
    //console.log("ici")
      this._user_id_li.style.display = "block";
      this._user_id.value = character.id;
      this._user_name.value = character.name;
      this._user_surname.value = character.surname;
      this._user_description.value = character.description;
      this._user_game.value = character.game;

      let listAllItem = await ItemFetch.getItemListAll();
      //this._user_items.value = character.items;
      let ul = document.createElement("ul");
      ul.id = "ulAllItem";
      for(const it of listAllItem){
        let li = document.createElement("li");
        let check = document.createElement("input");
        let label = document.createElement("label");
        check.id = it.id;
        check.setAttribute("type", "checkbox");
        label.innerText = it.name;
        for(const items of character.items){
          //console.log(it.id);
          //console.log(items.id);
          if(it.id === items){
            check.checked = true;
          }
        }
        li.appendChild(check);
        li.appendChild(label);
        ul.appendChild(li);
      }
      this.supItmListAll();
      this._itemList.appendChild(ul)

      this._user_level.value = character.level;
  }


  async viewClear(){
    //console.log("clear char");
    this._user_id_li.style.display = "none";
    this._user_id.value = "";
    this._user_name.value = "";
    this._user_surname.value = "";
    this._user_description.value = "";
    this._user_game.value = "";
    //this._user_items.value = "";
    this.supItmListAll();
    this._user_level.value = "";

    let listAllItem = await ItemFetch.getItemListAll();
    //this._user_items.value = character.items;
    let ul = document.createElement("ul");
    ul.id = "ulAllItem";
    for(const it of listAllItem){
      let li = document.createElement("li");
      let check = document.createElement("input");
      let label = document.createElement("label");
      check.id = it.id;
      check.setAttribute("type", "checkbox");
      label.innerText = it.name;
      li.appendChild(check);
      li.appendChild(label);
      ul.appendChild(li);
    }
    this.supItmListAll();
    this._itemList.appendChild(ul)
  }

  supItmListAll(){
    let listChildListLeft = this._itemList.childNodes;
    //console.log("clear?")
    for(const element of listChildListLeft){
      this._itemList.removeChild(element);
    }
  }

  async submit(){
    let listItem = [];
    let listChild = document.getElementById("ulAllItem").childNodes;
    for(const element of listChild){
      for(const ele of element.childNodes){
          //console.log(element);
          if(ele.nodeName === "INPUT"){
            if(ele.checked === true){
              listItem.push(parseInt(ele.id));
              //console.log(ele.id);
            }
          }
      }
    }
    //console.log(listItem);

    switch ("") {
    case this._user_name.value:
      alert("Champ 'Nom' requis");
      return;
    case this._user_surname.value:
      alert("Champ 'Surnom' requis");
      return;
    case this._user_description.value:
      alert("Champ 'Description' requis");
      return;
    case this._user_game.value:
      alert("Champ 'Jeu' requis");
      return;
    case this._user_level.value:
      alert("Champ 'Niveau' requis");
      return;
    }

    let character = new Character(this._user_id.value,
                              this._user_name.value,
                              this._user_surname.value,
                              this._user_description.value,
                              this._user_game.value,
                              listItem,//this._user_items.value,
                              this._user_level.value);

    if(this._user_id_li.style.display === "none"){
      await CharacterFetch.insertCharacter(character);
      console.log("submit ?")
    }else{
      await CharacterFetch.modifyCharacter(character);
      console.log("edit ?")
    }
    this.viewClear();
    this._listener.viewList();
  }
}

class FormItem {
  constructor(listener) {

    this._formItem = document.getElementById("formItem");
    this._item_id_li = document.getElementById("item_id_li");
    this._item_id = document.getElementById("item_id");
    this._item_name = document.getElementById("item_name");
    this._item_description = document.getElementById("item_description");
    this._item_game = document.getElementById("item_game");
    this._item_attackDamage = document.getElementById("item_attackDamage");
    this._item_effiency = document.getElementById("item_effiency");
    this._validateFormItem = document.getElementById("validateFormItem");

    const self = this;

    this._listener = listener;

    this._validateFormItem.onclick = (e) => self.submit();

  }

  viewClear(){
    console.log("clear Item");
    this._item_id_li.style.display = "none";
    this._item_id.value = "";
    this._item_name.value = "";
    this._item_description.value = "";
    this._item_game.value = "";
    this._item_attackDamage.value = "";
    this._item_effiency.value = "";
  }

  bindForm(){
      this._formItem.style.display = "none";
  }

  displayForm(){
    this._formItem.style.display = "block";
    this._item_id_li.style.display = "none";
  }

  del(){
    console.log("del " + this._item_id.value);
    ItemFetch.deleteItem(this._item_id.value);
  }

  viewAll(item){
    //console.log("ici")
      this._item_id_li.style.display = "block";
      this._item_id.value = item.id;
      this._item_name.value = item.name;
      this._item_description.value = item.description;
      this._item_game.value = item.game;
      this._item_attackDamage.value = item.attackDamage;
      this._item_effiency.value = item.effiency;

  }

  async submit(){
    //console.log("submit ?")

    switch ("") {
      case this._item_name.value:
        alert("Champ 'Nom' requis");
        return;
      case this._item_description.value:
        alert("Champ 'Description' requis");
        return;
      case this._item_game.value:
        alert("Champ 'Jeu' requis");
        return;
      case this._item_attackDamage.value:
        alert("Champ 'Dégâts' requis");
        return;
      case this._item_effiency.value:
        alert("Champ 'Efficacité' requis");
        return;
      }

    let item = new Item(this._item_id.value,
                              this._item_name.value,
                              this._item_description.value,
                              this._item_game.value,
                              this._item_attackDamage.value,
                              this._item_effiency.value
                            );
    if(this._item_id_li.style.display === "none"){
      await ItemFetch.insertItem(item);
      //console.log("submit ?")
    }else{
      await ItemFetch.modifyItem(item);
      //console.log("edit ?")
    }
    this.viewClear();
    this._listener.viewList();
  }




}
