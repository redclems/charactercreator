class ItemFetch {
    /**
     * Fetch the list of items
     *
     * @param {int} page the page
     * @param {AppCharacter} listener
     * @returns {Array<Item>} the list of item
     */
    static async getItemList(page, listener) {
        const nbItemByPage = 5;
        const res = await fetch("http://localhost:4000/items", {
            method: "GET",
        }).then(response => response.json());

        let itemList = [];
        let nb = 0;
        for (const i of res){
          if(nb >= nbItemByPage*page && nb < nbItemByPage + (nbItemByPage*page)){
            itemList.push(new Item(i.id, i.name, i.description, i.game, i.attackDamage, i.effiency, i.uri));
          }
          nb++;
        }
        let plus = 0;
        if(nb%nbItemByPage != 0){
          plus++;
        }

        listener.pageBp((nb/nbItemByPage) +plus);

        return itemList;
    }
    /**
     * Fetch the list of items
     *
     * @returns {Array<Item>} the list of item
     */
    static async getItemListAll() {
        const res = await fetch("http://localhost:4000/items", {
            method: "GET",
        }).then(response => response.json());

        let itemList = [];
        for (const i of res){
            itemList.push(new Item(i.id, i.name, i.description, i.game, i.attackDamage, i.effiency, i.uri));
        }

        return itemList;
    }

    /**
     * Fetch one item depending on its ID
     *
     * @param {int} id the id of the item
     * @returns {Item} the corresponding item
     */
    static async getItemById(id) {
        const i = await fetch("http://localhost:4000/items/"+id, {
            method: "GET",
        }).then(response => response.json());

        return new Item(i.id, i.name, i.description, i.game, i.attackDamage, i.effiency, i.uri);
    }

    /**
     * Fetch all item depending on its research
     * @param {int} page the page
     * @param {AppCharacter} listener
     * @param {string} research the research of the item
     * @returns {Item} the corresponding item
     */
    static async getItemResearch(research, page, listener) {
        const nbItemByPage = 5;
        const res = await fetch("http://localhost:4000/items", {
            method: "GET",
        }).then(response => response.json());

        let itemList = [];
        let nb = 0;
        for (const i of res) {
          if(nb >= nbItemByPage*page && nb < nbItemByPage + (nbItemByPage*page)){
            if(i.name === research || i.game === research || i.id === research || research == ""){
              itemList.push(new Item(i.id, i.name, i.description, i.game, i.attackDamage, i.effiency, i.uri));
            }
          }
          nb++;
        }

        let plus = 0;
        if(nb%nbItemByPage != 0){
          plus++;
        }
        listener.pageBp((nb/nbItemByPage) + plus);

        return itemList;
    }

    /**
     * Inserts a new item
     *
     * @param {Item} item
     */
    static async insertItem(item) {
        item.newURI(await this.getMaxId());
        const c = await fetch("http://localhost:4000/items", {
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json"
            },
            method: "POST",
            body: JSON.stringify(item)
        }).then(response => {
            if (response.ok) {
                console.log("Save Success");
            }
            else throw new Error("Ajax error: "+response.status);
        })
        .catch(response => {console.log(response)});
    }

    /**
     * Deletes one item depending on its ID
     *
     * @param {int} id the id of the item
     * @returns {Promise<Item>} the item that has been deleted
     */
    static async deleteItem(id) {
        return fetch("http://localhost:4000/items/"+id, {
            method: "DELETE",
        }).then(response => response.json());
    }

    /**
     * Fetch the item and returns the highest ID
     *
     * @returns {int} the highest ID
    */
    static async getMaxId() {
        const res = await fetch("http://localhost:4000/items", {
            method: "GET",
        }).then(response => response.json());

        let itemList = [];
        for (const i of res) itemList.push(i.id);

        console.log(Math.max(itemList));
        return Math.max.apply(null,itemList)
    }

    /**
     * Replaces the item that has the same ID as the parameter
     *
     * @param {Item} item
     */
     static async modifyItem(item) {
        console.log(JSON.stringify(item));
        const c = await fetch("http://localhost:4000/items/"+item.id, {
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json"
            },
            method: "PUT",
            body: JSON.stringify(item)
        }).then(response => {
            if (response.ok) {
                console.log("Save Success");
            }
            else throw new Error("Ajax error: "+response.status);
        })
        .catch(response => {console.log(response)});
    }
}

class Item {
    constructor(idItem, newName, newDescription, newGame, newAttackDamage, newEffiency, newUri) {
        this.id = idItem;
        this.name = newName;
        this.description = newDescription;
        this.game = newGame;
        this.attackDamage = newAttackDamage;
        this.effiency = newEffiency;
        this.uri = newUri;
    }

    get newName() {
        return this.name;
    }

    get newDescription() {
        return this.description;
    }

    get newGame() {
        return this.game;
    }

    get newAttackDamage() {
        return this.attackDamage;
    }

    get newEffiency() {
        return this.effiency;
    }

    get newUri() {
        return this.uri;
    }

    async newURI(id) {
        this.uri += id+1;
    }
}
